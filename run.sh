
TRAINFILE=/eos/user/m/mguth/public/btagging-ml_tutorial_files/MC16d_hybrid-training_sample-NN.h5
VALIDFILE=/eos/user/m/mguth/public/btagging-ml_tutorial_files/MC16d_hybrid_odd_100_PFlow-validation.h5
VARS=DL1_framework/Training/configs/DL1r_Variables.json
SCALES=DL1_framework/Preprocessing/dicts/params_MC16D-ext_2018-PFlow_70-8M_mu.json
python train.py  --epochs 1 --large_file --configs $(ls configs/HP_config*.json|paste -d, -s -) --validationfile ${VALIDFILE} --validation_config ${SCALES} --variables ${VARS} --outputfile out.json --trainingfile ${TRAINFILE}